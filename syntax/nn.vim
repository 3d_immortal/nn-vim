if exists("b:current_syntax")
  finish
endif

syn keyword nnCond if orif or case
syn keyword nnLoop for fore
syn keyword nnStruct class enum
syn keyword nnInc inc imp
syn keyword nnType u8 u16 u32 u64
syn keyword nnType s8 s16 s32 s64 float double
syn keyword nnType bool f32 f64 char short int unsigned size_t long signed
syn keyword nnKeyWord fn ctor dtor cpy mov test ret nil exe cflags lflags
syn keyword nnKeyWord fna fnv dtora dtorv op opa opv sizeof static_cast
syn keyword nnKeyWord const_cast reinterpret_cast cexpr typename
syn keyword nnKeyWord def del new ns brk cont use this static static_read
syn keyword nnFunc echo echoln expecty expectn expecteq asserty assertn asserteq
syn keyword nnFunc assert dassert error errorln assertnear expectnear
syn keyword nnBool yay nay
syn match nnDollar "\v\$"

hi def link nnCond Conditional
hi def link nnLoop Repeat
hi def link nnStruct Structure
hi def link nnInc Include
hi def link nnType Type
hi def link nnKeyWord Keyword
hi def link nnFunc Function
hi def link nnBool Boolean
hi def link nnDollar Keyword

syn match nnBraces       "[{}\[\]]"
syn match nnParens       "[()]"
syn match nnOpSymbols    "=\{1,2}\|!=\|<\|>\|>=\|<=\|++\|+=\|--\|-=\|-\|#\|%\|\^\|:"
syn match nnOpSymbols    "\(\*\)\|\(|\)\|\(&\)\|\(?\)\|\(+\)"
syn match nnOpSymbols   "\v\s/\s"
syn match nnEndColons    "[,;]"
syn match nnLogicSymbols "\(&&\)\|\(||\)\|\(!\)"
syn match nnAt "\v\@"
syn match nnQualifier "\v\~"

hi def link nnBraces Delimiter
hi def link nnParens Delimiter
hi def link nnOpSymbols Operator
hi def link nnEndColons Delimiter
hi def link nnLogicSymbols Operator
hi def link nnAt Label
hi def link nnAt Label
hi def link nnQualifier StorageClass

syn match nnClass "\v<\u\w*>"
syn match nnMacroConst "\u\(\u\|_\|\d\)\+\>"
syn match nnEnumConsts "\v<k(\u|\d)\w*>"
syn match nnEnumType "\v<E(\u)\w*>"
syn match nnField "\v\w(\w)*_>"
syn match nnField "\v<m\u\w*>"
syn match nnField "\v<_\l\w*>"
syn match nnFuncCall "\v<\l\w*\ze\("
syn match nnFuncCall "\v<\u\w*\ze\("

hi def link nnField Field
hi def link nnFuncCall FuncCall
hi def link nnClass UserType
hi def link nnMacroConst MacroConst
hi def link nnEnumConsts EnumConst
hi def link nnEnumType EnumType

syn match nnNumber "\v<\d+>"
syn match nnNumber "\v<0x\w+>"
hi def link nnNumber Number

syn match nnFloat "\v<(\d*)\.(\d*)(f?)>"
syn match nnFloat "\v<\.(\d*)(f?)>"
hi def link nnFloat Float

syn keyword nnTodo contained TODO FIXME
syn match nnComment "\v\/\/.*$" contains=nnTodo,@spell
syn region nnComment start="/\*" end="\*/" contains=nnTodo,@Spell
hi def link nnComment Comment
hi def link nnTodo Todo

syn region nnString start=/\v"/ skip=/\v\\./ end=/\v"/
syn region nnRawString start=/\v`/ skip=/\v\\./ end=/\v`/
hi def link nnString String
hi def link nnRawString String

syn region nnChar start=/\v'/ skip=/\v\\./ end=/\v'/
hi def link nnChar Character

let b:current_syntax = "nn"
